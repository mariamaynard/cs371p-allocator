// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream>
#include "Allocator.hpp"

using namespace std;

/*
num_test_cases
blank
test case until blank line
blank
test case...
*/
//read in the test cases and run them
void allocator_read() {
    int numCases;
    cin >> numCases;
    assert(numCases > 0);
    string line;
    getline(cin, line); //clear the first line
    getline(cin, line); //blank line
    for(int cases = 0; cases < numCases; ++cases) {
        my_allocator<double, 1000> allocator;
        while(getline(cin,line) && line != "") {
            int request;
            istringstream strin(line);
            strin >> request;
            if(request >= 0) {
                // allocate
                allocator.allocate(request);
            } else {
                // deallocate
                // -24 30 -12  20 100
                my_allocator<double, 1000>::iterator strt = allocator.begin();
                my_allocator<double, 1000>::iterator end = allocator.end();
                int busy = 0;
                while(busy < abs(request) && strt != end)  {
                    if(*strt < 0) {
                        // the block is busy
                        ++busy;
                        if (busy == abs(request)) {
                            break;
                        }
                    }
                    strt++;
                }
                allocator.deallocate(reinterpret_cast<double*>(&(*strt)) + 1, sizeof(double));
            }
        }
        // allocate of 0 should be bad alloc
        my_allocator<double, 1000>::iterator itr = allocator.begin();
        my_allocator<double, 1000>::iterator itrEnd = allocator.end();
        my_allocator<double, 1000>::iterator finalElem = allocator.end();
        --finalElem;
        // cout << "start is: " << &(*itr) << " end is: " << &(*finalElem) << endl;
        // print out the final answer by going through the lines
        while(&(*itr) != &(*itrEnd)) {
            // cout << "end is: " << *finalElem << endl;
            // cout << "at : " << &(*itr) << endl;
            if(&(*itr) != &(*finalElem)) {
                cout << *itr << " ";
                // cout << "HERE";
            }
            else {
                // don't add the space at the end
                // cout << "END";
                cout << *itr;
            }
            ++itr;
        }
        cout << endl; //new line for output
    }
}

// ----
// main
// ----
int main () {
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */
    allocator_read();
    return 0;
}


