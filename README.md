# CS371p: Object-Oriented Programming Allocator Repo

* Name: Maria Maynard and Joseph Muffoletto

* EID: mm87445 and jrm7925

* GitLab ID: mariamaynard and joemuff999

* HackerRank ID: maria_maynard213

* Git SHA: 1a031ba20d008e31d7e0c72650f4aa397080d229

* GitLab Pipelines: https://gitlab.com/mariamaynard/cs371p-allocator/-/pipelines

* Estimated completion time: 6 hours

* Actual completion time: 6 hours

* Comments: We worked together more than 75% of the time
