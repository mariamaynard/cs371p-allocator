// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <gtest/gtest_prod.h> //friend test
// ---------
// Allocator
// ---------
using namespace std;
template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // friend tests
    // -----------
    FRIEND_TEST(FriendTestFixture, test25);

    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return *lhs == *rhs;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            int currVal = abs(*_p);
            _p = (int*) ((char*) _p + currVal + 2 * sizeof(int));
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------
        /*  */
        iterator& operator -- () {
            int* prevEnd = (int*) ((char*) _p - sizeof(int));
            // cout << *prevEnd << endl;
            int prevSize = *prevEnd;
            _p = (int*) ((char*) _p - abs(prevSize) - 2 * sizeof(int));
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return *lhs == *rhs;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            int currVal = abs(*_p);
            // cout << "p before " << _p << endl;

            _p = (int*) ((char*) _p + currVal + 2 * sizeof(int));
            // cout << "p after " << _p << endl;
            // cout << currVal  << endl;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            int* prevEnd = (int*) ((char*) _p - sizeof(int));
            int prevSize = *prevEnd;
            _p = (int*) ((char*) _p - (prevSize) -2 * sizeof(int));
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     */
    bool valid () const {
        auto s = begin();
        auto e = (int*)(reinterpret_cast<const char*>(&(*s)) + N);
        unsigned int currSize;
        unsigned int currByte = 0;
        unsigned int totalBytes = 0;
        while(&(*s) != &(*e)) {
            // cout << "s is : " << s;
            // cout << "e is : " << e;
            currSize = abs(*s);
            // cout << *s << " number is" << endl;
            totalBytes+= currSize + 8;
            // std::cout << "block size in valid = " << *s << std::endl;
            assert(currSize < N);
            assert(currSize >= sizeof(T));
            // cout << "curr " << (*this)[currByte] << " end " << (*this)[currByte+currSize+sizeof(int)] << endl;
            assert((*this)[currByte] == (*this)[currByte+currSize+sizeof(int)]);
            currByte += currSize + 8;
            ++s;
        }
        assert(totalBytes == N);
        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // this is code from class
        (*this)[0] = N-8;
        (*this)[N-4] = N-8;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type s) {
        if(s <= 0) //check if trying to allocate 0
        {
            throw std::bad_alloc();
            return nullptr;
        }
        iterator itr = begin();
        // iterator endItr = end();
        int byte_index = 0;
        int*  end = (int*)(reinterpret_cast<char*>(&(*itr)) + N);
        while(&(*itr) != end) {
            // cout << *itr << endl;
            if(*itr < 0)
            {
                // busy block
                byte_index += abs(*itr)+8;
                ++itr;
                continue;
            }
            else if((long unsigned int) *itr >= sizeof(T) * s) {
                // cout << "found block " << endl;
                // use this block
                int leftovers = *itr - (sizeof(T) * s);
                assert(leftovers >= 0);
                if((long unsigned int) leftovers < 8 + sizeof(T)) {
                    // dont split
                    int block_size = -1 * ((*this)[byte_index]);
                    // cout << "no split: " << block_size << endl;
                    (*this)[byte_index] = block_size;
                    (*this)[byte_index + abs(block_size) + sizeof(int)] = block_size;
                    assert(valid());
                    return (T*) &((*this)[byte_index + sizeof(int)]);
                }
                // split the block
                //first block
                // cout << "split" << endl;
                (*this)[byte_index] = -1 * sizeof(T) * s;
                (*this)[byte_index + sizeof(T) * s + sizeof(int)] = -1 * sizeof(T) * s;
                //update the second block
                unsigned long int second_block_sentinel = leftovers - 2*sizeof(int);
                (*this)[byte_index + sizeof(T) * s + 2*sizeof(int)] = second_block_sentinel;
                (*this)[byte_index + sizeof(T) * s + 3*sizeof(int) + second_block_sentinel] = second_block_sentinel;
                assert(valid());
                return (T*) &((*this)[byte_index + sizeof(int)]);

            }
            byte_index += *itr+8;
            // cout << "byte index : " << byte_index <<endl;
            ++itr;
        }
        throw std::bad_alloc();
        return nullptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     */
    void deallocate (pointer p, size_type) {
        // in bound
        // a busy block
        p -= 1;
        int start_index = (char*)p - (char*)a;
        if(((char *) p < a || (char *) p > ((char *) a + N)) || (*this)[start_index] > 0)
        {
            throw invalid_argument("argument p is out of bounds or not a valid busy block");
        }
        // a...a b...b c...c

        int block_size = abs((*this)[start_index]);

        int end_index = block_size + start_index + sizeof(int);
        if(block_size == N - 8)
        {
            // only block here
            (*this)[start_index] = block_size;
            (*this)[end_index] = block_size;
        }
        else if(start_index == 0)
        {
            // first block so we only need to check the right side
            auto rightSize = (*this)[start_index + 2 * sizeof(int) + block_size];
            if(rightSize > 0) //check if free block so we can coalesce
            {
                // free to the right a..a b..b -> a+b+8...a+b+8
                block_size += rightSize + 2 * sizeof(int);
            }
            end_index = start_index + block_size + sizeof(int);
            (*this)[0] = block_size;
            (*this)[end_index] = block_size;
        }
        else if(end_index == N-sizeof(int))
        {
            // the last block only check left side
            auto leftSize = (*this)[start_index - sizeof(int)];
            if(leftSize > 0) {
                start_index = start_index - 2 * sizeof(int) - leftSize;
                block_size += leftSize + 2 * sizeof(int);
            }
            end_index = start_index + block_size + sizeof(int);
            (*this)[start_index] = block_size;
            (*this)[end_index] = block_size;
        }
        else
        {
            // we are somewhere in the middle of the list so we should check both sides
            auto leftSize = (*this)[start_index - sizeof(int)];
            auto rightSize = (*this)[start_index + 2 * sizeof(int) + block_size];
            if(leftSize > 0)
            {

                start_index = start_index - 2 * sizeof(int) - leftSize;
                block_size += leftSize + 2 * sizeof(int);
                (*this)[start_index] = block_size;
                (*this)[end_index] = block_size;


            }
            if(rightSize > 0)
            {

                // free to the right a..a b..b -> a+b+8...a+b+8
                block_size += rightSize + 2*sizeof(int);
                end_index = start_index + block_size + sizeof(int);

            }
            (*this)[start_index] = block_size;
            (*this)[end_index] = block_size;

        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
