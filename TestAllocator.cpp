// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator


#include "Allocator.hpp"
#include "gtest/gtest.h"


TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}


TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;
    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);
}

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;
    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);
}
/*
iterator:
for const and non-const
constructor(int* p), *, ++ (post/pre), -- (post/pre)

allocator:
 ==, !=
allocate, deallocate
 valid - use FRIEND_TEST (gtest macro) https://piazza.com/class/kjopekst1mv6oo?cid=219

begin and end?
*/

// checking end sentinal after construction
TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;
    allocator_type x;
    ASSERT_EQ(x[996], 992);
}

//testing == for allocators
TEST(AllocatorFixture, test5) {
    my_allocator<double, 1000> allocator1;
    my_allocator<double, 1000> allocator2;
    ASSERT_FALSE(allocator1 == allocator2);
}

//testing != for allocators
TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<int, 1000>;
    allocator_type alloc1;
    allocator_type alloc2;
    ASSERT_TRUE(alloc1 != alloc2);
}

// testing == for iterator
TEST(AllocatorFixture, test7) {
    my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::iterator itr2 = allocator.begin();
    ASSERT_TRUE(itr1 == itr2);
}

//testing != for iterator
TEST(AllocatorFixture, test8) {
    my_allocator<double,1000> allocator;
    allocator.allocate(1);
    my_allocator<double, 1000>::iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::iterator itr2 = allocator.begin();
    ++itr2;
    ASSERT_TRUE(itr1 != itr2);
}

//testing constructor with pointer passed in
TEST(AllocatorFixture, test9) {
    my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::iterator itr0 = allocator.begin();
    my_allocator<double, 1000>::iterator itr1(itr0);
    my_allocator<double, 1000>::iterator itr2 = allocator.begin();
    ASSERT_TRUE(itr1 == itr2);
}


//testing post-increment for iterator
TEST(AllocatorFixture, test10) {
    my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::iterator itr2 = allocator.end();
    itr1++;
    ASSERT_TRUE(itr1 == itr2);
}

//testing pre-increment for iterator
TEST(AllocatorFixture, test11) {
    my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::iterator itr2 = allocator.end();
    ++itr1;
    ASSERT_TRUE(itr1 == itr2);
}

//testing post-decrement for iterator
TEST(AllocatorFixture, test12) {
    my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::iterator itr2 = allocator.end();
    itr2--;
    ASSERT_TRUE(itr1 == itr2);
}

//testing pre-decrement for iterator
TEST(AllocatorFixture, test13) {
    my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::iterator itr2 = allocator.end();
    --itr2;
    ASSERT_TRUE(itr1 == itr2);
}

// testing == for const iterator
TEST(AllocatorFixture, test14) {
    const my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::const_iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::const_iterator itr2 = allocator.end();
    --itr2;
    ASSERT_TRUE(itr1 == itr2);
}

//testing != for const iterator
TEST(AllocatorFixture, test15) {
    const my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::const_iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::const_iterator itr2 = allocator.begin();
    ASSERT_FALSE(itr1 != itr2);
}

//testing const iterator constructor with pointer passed in
TEST(AllocatorFixture, test16)  {
    const my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::const_iterator itr0 = allocator.begin();
    my_allocator<double, 1000>::const_iterator itr1(itr0);
    my_allocator<double, 1000>::const_iterator itr2 = allocator.begin();
    ASSERT_TRUE(itr1 == itr2);
}


//testing post-increment for const iterator
TEST(AllocatorFixture, test17)  {
    const my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::const_iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::const_iterator itr2 = allocator.end();
    itr1++;
    ASSERT_TRUE(itr1 == itr2);
}

//testing pre-increment for const iterator
TEST(AllocatorFixture, test18) {
    const my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::const_iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::const_iterator itr2 = allocator.end();
    ++itr1;
    ASSERT_TRUE(itr1 == itr2);
}

//testing post-decrement for const iterator
TEST(AllocatorFixture, test19) {
    const my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::const_iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::const_iterator itr2 = allocator.end();
    itr2--;
    ASSERT_TRUE(itr1 == itr2);
}

//testing pre-decrement for const iterator
TEST(AllocatorFixture, test20) {
    const my_allocator<double,1000> allocator;
    my_allocator<double, 1000>::const_iterator itr1 = allocator.begin();
    my_allocator<double, 1000>::const_iterator itr2 = allocator.end();
    --itr2;
    ASSERT_TRUE(itr1 == itr2);
}

//testing allocate: simple allocation
TEST(AllocatorFixture, test21) {
    my_allocator<double,1000> allocator;
    allocator.allocate(1);
    ASSERT_EQ(allocator[0], -8);
    ASSERT_EQ(allocator[16], 976);
}

//testing allocate: multiple allocation
TEST(AllocatorFixture, test22) {
    my_allocator<double,1000> allocator;
    allocator.allocate(1);
    allocator.allocate(1);
    allocator.allocate(2);
    ASSERT_EQ(allocator[0], -8);
    ASSERT_EQ(allocator[16], -8);
    ASSERT_EQ(allocator[32], -16);
}

//testing allocate: big allocate
TEST(AllocatorFixture, test23) {
    my_allocator<double,1000> allocator;
    allocator.allocate(100);
    ASSERT_EQ(allocator[0], -800);
}

//testing deallocate: simple
TEST(AllocatorFixture, test24) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;
    allocator_type   x;
    pointer p = x.allocate(1);
    x.deallocate(p, sizeof(int));
    ASSERT_EQ(x[0], 992);
}

//testing valid on a valid set of inputs
TEST(FriendTestFixture, test25)
{
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    // *((int*)allocator.a) = 100000; //invalid data
    allocator_type allocator;

    pointer p = allocator.allocate(5);
    allocator.deallocate(p, sizeof(int));
    ASSERT_TRUE(allocator.valid());
}